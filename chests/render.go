package main

import (
	"flag"
	"fmt"
	"github.com/fogleman/gg"
	. "github.com/seanpringle/spt"
	"log"
	"math"
)

var (
	storage         = Metal(Hex(0xe4b730), 0.99)
	buffer          = Metal(Hex(0x4cc668), 0.99)
	requester       = Metal(Hex(0x57adcd), 0.99)
	activeProvider  = Metal(Hex(0x9936c8), 0.99)
	passiveProvider = Metal(Hex(0xda403c), 0.99)
	renderers       []Renderer
)

var chests = map[string][]Material{
	(`chest%dx%d`):                  []Material{Copper, Steel},
	(`chest%dx%d-storage`):          []Material{Steel, storage},
	(`chest%dx%d-buffer`):           []Material{Steel, buffer},
	(`chest%dx%d-requester`):        []Material{Steel, requester},
	(`chest%dx%d-active-provider`):  []Material{Steel, activeProvider},
	(`chest%dx%d-passive-provider`): []Material{Steel, passiveProvider},
}

func main() {
	var dotech = flag.Bool("tech", false, "tech icons")
	var doicon = flag.Bool("icon", false, "item icons")
	flag.Parse()

	renderers = []Renderer{
		//NewLocalRenderer(),
		NewRPCRenderer(`slave1:34242`),
		NewRPCRenderer(`slave2:34242`),
		NewRPCRenderer(`slave3:34242`),
		NewRPCRenderer(`slave4:34242`),
		NewRPCRenderer(`slave5:34242`),
		NewRPCRenderer(`slave6:34242`),
		NewRPCRenderer(`slave7:34242`),
		NewRPCRenderer(`slave8:34242`),
	}

	if *dotech {
		RenderSave("tech1.png", scene(V3(0, 0, 300), 1000000, 0.055, false, chest(Copper, Steel, 950, 900, 600, 0.5), 128, 128), renderers)
		RenderSave("tech2.png", scene(V3(0, 0, 300), 1000000, 0.055, false, chest(Steel, passiveProvider, 950, 900, 600, 0.5), 128, 128), renderers)
		RenderSave("tech3.png", scene(V3(0, 0, 300), 1000000, 0.055, false, chest(Steel, requester, 950, 900, 600, 0.5), 128, 128), renderers)
	}

	for pattern, materials := range chests {
		if *doicon {
			for pass := range Render(scene(V3(0, 0, 300), 1000000, 0.05, false, chest(materials[0], materials[1], 950, 900, 600, 0.0), 64, 64), renderers) {
				canvas := gg.NewContext(64, 64)
				for i := 1; i < 10; i++ {
					canvas.SetRGBA(0, 0, 0, 0)
					canvas.Clear()
					canvas.DrawImage(pass, 0, 0)
					if err := canvas.LoadFontFace(`Roboto-Bold.ttf`, 28); err != nil {
						log.Println(err)
					}
					canvas.SetRGBA(1, 1, 1, 1)
					canvas.DrawStringAnchored(fmt.Sprintf("%d", i), 32, 32, 0.5, 0)
					gg.SavePNG(fmt.Sprintf(pattern+`-icon.png`, i, i), canvas.Image())
				}
			}
		}
		RenderSave(fmt.Sprintf(pattern+`.png`, 1, 1), scene(V3(0, 0, 300), 1000000, 0.1635, true, chest(materials[0], materials[1], 950, 900, 600, 0.5), 256, 256), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 2, 2), scene(V3(0, 50, 300), 1000000, 0.1635, true, chest(materials[0], materials[1], 1950, 2200, 700, 0.75), 256, 256), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 3, 3), scene(V3(0, 100, 300), 1000000, 0.24375, true, chest(materials[0], materials[1], 2950, 3500, 800, 1.25), 384, 384), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 4, 4), scene(V3(0, 150, 300), 1000000, 0.24375, true, chest(materials[0], materials[1], 3950, 4800, 900, 1.5), 384, 384), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 5, 5), scene(V3(0, 200, 300), 1000000, 0.325, true, chest(materials[0], materials[1], 4950, 6100, 1000, 1.75), 512, 512), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 6, 6), scene(V3(0, 250, 300), 1000000, 0.325, true, chest(materials[0], materials[1], 5950, 7400, 1100, 2.0), 512, 512), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 7, 7), scene(V3(0, 300, 300), 5000000, 0.097, true, chest(materials[0], materials[1], 6950, 8700, 1200, 2.25), 768, 768), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 8, 8), scene(V3(0, 350, 300), 5000000, 0.097, true, chest(materials[0], materials[1], 7950, 10000, 1300, 2.5), 768, 768), renderers)
		RenderSave(fmt.Sprintf(pattern+`.png`, 9, 9), scene(V3(0, 400, 300), 5000000, 0.097, true, chest(materials[0], materials[1], 8950, 11300, 1400, 2.75), 768, 768), renderers)
	}
}

func lookFrom(z float64) Vec3 {
	return V3(0, -z, z)
}

func scene(center Vec3, dist, fov float64, shadow bool, model []Thing, width, height int) Scene {

	scene := Scene{
		Width:     width,
		Height:    height,
		Passes:    300,
		Samples:   10,
		Bounces:   4,
		Horizon:   10000000,
		Threshold: 0.0001,
		Ambient:   White.Scale(0.1),
		ShadowH:   0.9,
		ShadowL:   0.1,
		ShadowD:   0.25,
		ShadowR:   0.1,

		Camera: NewCamera(
			lookFrom(dist),
			center,
			Z3,
			fov,
			Zero3,
			0.0,
		),

		Stuff: append(
			[]Thing{
				Object(
					Light(White.Scale(3)),
					RotateY(10, Translate(V3(-500000, 0, 1000000), Sphere(500000))),
				),
			},
			model...,
		),
	}

	if shadow {
		scene.Stuff = append([]Thing{SpaceTime(25000)}, scene.Stuff...)
	}

	return scene
}

func gear() SDF3 {
	teeth := []SDF3{}
	for i := 0; i < 24; i++ {
		teeth = append(teeth,
			RotateZ(float64(i)*(360.0/24.0), TranslateX(480,
				Distort(V3(1, 0.35, 1), Intersection(
					Cylinder(100, 110),
					Cube(200, 200, 100),
				)),
			)),
		)
	}
	return Union(append([]SDF3{Cylinder(100, 500)}, teeth...)...)
}

func logoBase() SDF3 {
	return Difference(
		TranslateZ(50, gear()),
		TranslateZ(100, Cylinder(55, 425)),
	)
}

func logoCrux() SDF3 {
	bigStar := Extrude(30, Hexagram(30))
	smallStar := Extrude(30, Hexagram(20))
	return RotateZ(-10, Union(
		Translate(V3(0, 300, 85), bigStar),
		Translate(V3(0, -300, 85), bigStar),
		Translate(V3(-225, 50, 85), bigStar),
		Translate(V3(225, 80, 85), bigStar),
		Translate(V3(100, -90, 85), smallStar),
	))
}

const (
	bodyRadius  = 100.0
	cornerSize  = 200.0
	poleRadius  = 25.0
	inlayRadius = 25.0
	inlayStep   = 200.0
)

func corners(bodyWidth, bodyDepth float64) SDF3 {
	w := cornerSize
	x := bodyWidth/2 - w/2.25
	y := bodyDepth/2 - w/2.25
	c := Difference(
		Cube(w, w, w),
		Translate(V3(w/2, w/2, 0), RotateZ(45, Cube(w*2, w, w*2))),
		Translate(V3(w/2, 0, -w/2), RotateY(45, Cube(w*2, w*2, w))),
		Translate(V3(0, w/2, -w/2), RotateX(45, Cube(w*2, w, w*2))),
	)
	return Union(
		Translate(V3(x, y, 0), RotateZ(180, c)),
		Translate(V3(x, -y, 0), RotateZ(270, c)),
		Translate(V3(-x, y, 0), RotateZ(90, c)),
		Translate(V3(-x, -y, 0), RotateZ(0, c)),
	)
}

func vpoles(bodyWidth, bodyDepth, bodyHeight float64) SDF3 {
	r := poleRadius
	x := bodyWidth/2 - r*0.75
	y := bodyDepth/2 - r*0.75
	return Union(
		Translate(V3(x, y, 0), Cylinder(bodyHeight, r)),
		Translate(V3(x, -y, 0), Cylinder(bodyHeight, r)),
		Translate(V3(-x, y, 0), Cylinder(bodyHeight, r)),
		Translate(V3(-x, -y, 0), Cylinder(bodyHeight, r)),
	)
}

func hpoles(bodyWidth, bodyDepth float64) SDF3 {
	r := poleRadius
	x := -bodyWidth/2 + r*0.75
	y := -bodyDepth/2 + r*0.75
	return Union(
		TranslateX(x, RotateX(90, Cylinder(bodyDepth, r))),
		RotateZ(90, TranslateX(y, RotateX(90, Cylinder(bodyWidth, r)))),
		RotateZ(180, TranslateX(x, RotateX(90, Cylinder(bodyDepth, r)))),
		RotateZ(270, TranslateX(y, RotateX(90, Cylinder(bodyWidth, r)))),
	)
}

func dpoles(bodyWidth, bodyDepth float64) SDF3 {
	r := poleRadius
	l := math.Sqrt(bodyWidth*bodyWidth + bodyDepth*bodyDepth)
	return Union(
		RotateZ(45, RotateX(90, Cylinder(l, r))),
		RotateZ(45, RotateX(90, Cylinder(l, r))),
	)
}

func sideGrid(bodyWidth, bodyDepth, bodyHeight float64) SDF3 {
	c := CylinderR(bodyHeight-bodyRadius*4, inlayRadius, inlayRadius)
	y := -bodyDepth / 2
	var items []SDF3
	for i := 0.0; i < bodyWidth/2-inlayStep; i += inlayStep {
		items = append(items, TranslateX(i, c))
		items = append(items, TranslateX(-i, c))
	}
	return TranslateY(y, Union(items...))
}

func grating(bodyWidth, bodyDepth, bodyHeight, scale float64) SDF3 {
	n := 250.0 * scale
	x := bodyWidth - n*2
	y := n * 1.6
	z := 50.0
	//r := bodyDepth / bodyWidth
	c := RotateY(-60, CylinderR(bodyHeight-bodyRadius*4, inlayRadius, inlayRadius))
	var items = []SDF3{CubeR(x, y, z, 10)}
	for i := 0.0; i < x/2-inlayStep; i += inlayStep {
		items = append(items, TranslateX(i-inlayRadius, Elongate(0, y/2, 0, c)))
		items = append(items, TranslateX(-i-inlayRadius, Elongate(0, y/2, 0, c)))
	}
	return Translate(V3(0, -bodyDepth/2+y/2+n, bodyHeight+z/2), Difference(items...))
}

func chest(trim, body Material, bodyWidth, bodyDepth, bodyHeight, logoScale float64) []Thing {
	var stuff []Thing
	if logoScale > 0.1 {
		stuff = append(stuff,
			Object(
				trim,
				TranslateZ(bodyHeight, Scale(logoScale, logoBase())),
			),
			Object(
				Matt(White),
				TranslateZ(bodyHeight, Scale(logoScale, logoCrux())),
			),
		)
	}
	if logoScale > 1.25 {
		stuff = append(stuff,
			Object(
				trim,
				Union(
					grating(bodyWidth, bodyDepth, bodyHeight, logoScale),
					MirrorY(grating(bodyWidth, bodyDepth, bodyHeight, logoScale)),
				),
			),
		)
	}
	stuff = append(stuff,
		Object(
			body,
			TranslateZ(bodyHeight/2, CubeR(bodyWidth, bodyDepth, bodyHeight, bodyRadius)),
		),
		Object(
			body,
			TranslateZ(bodyHeight/2, Union(
				sideGrid(bodyWidth, bodyDepth, bodyHeight),
			)),
		),
		Object(
			trim,
			TranslateZ(bodyHeight/2, Union(
				vpoles(bodyWidth, bodyDepth, bodyHeight),
				TranslateZ((bodyHeight-poleRadius*0.75)/2, hpoles(bodyWidth, bodyDepth)),
				TranslateZ(-(bodyHeight-poleRadius*0.75)/2, hpoles(bodyWidth, bodyDepth)),
			)),
		),
		Object(
			trim,
			TranslateZ(bodyHeight/2, Union(
				TranslateZ((bodyHeight-cornerSize*0.75)/2, Round(5, corners(bodyWidth, bodyDepth))),
				TranslateZ(-(bodyHeight-cornerSize*0.75)/2, RotateX(180, Round(5, corners(bodyWidth, bodyDepth)))),
			)),
		),
	)
	return stuff
}
